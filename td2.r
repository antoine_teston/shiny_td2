#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#
# ----------------------------------------------------------------------------

# Load packages
# ----------------------------------------------------------------------------
library(shiny)
library(dplyr)
library(ggplot2)

# Load data
# ----------------------------------------------------------------------------
patient = read.csv2("data/patient.csv")
location = read.csv2("data/location.csv")
hospital_stay = read.csv2("data/hospital_stay.csv")
hospital = read.csv2("data/hospital.csv")

complete_df = patient %>%
    merge(location, by = "location_id") %>%
    merge(hospital_stay, by = "patient_id") %>%
    merge(hospital, by = "hospital_id")

str(complete_df)

# Data management
# ----------------------------------------------------------------------------

    
# User Interface
# ----------------------------------------------------------------------------

# Define UI for application that draws a histogram
ui <- fluidPage(

    # Application title
    titlePanel("TD 2 - Printing a plot according to a filter"),

    # Sidebar with a slider input for number of bins 
    sidebarLayout(
        sidebarPanel(
            checkboxGroupInput("selected_locations", "Patients from :",
                               c(location$location))
        ),

        # Show a plot of the generated distribution
        mainPanel(
            tableOutput("data"),
            plotOutput("barplot")
        )
    )
)

# Server
# ----------------------------------------------------------------------------

# Define server logic required to draw a histogram
server <- function(input, output) {

    output$data <- renderTable({
        print(input$variable2)
        
        complete_df %>%
            filter(location %in% input$selected_locations) %>%
            group_by(hospital) %>%
            summarize(number_stays = n())
        
    }, rownames = FALSE)
    
    output$barplot <- renderPlot({
        print(input$variable2)
        
        complete_df %>%
            filter(location %in% input$selected_locations) %>%
            group_by(hospital) %>%
            summarize(number_stays = n()) %>%
            ggplot(aes(x = hospital, y = number_stays)) +
            geom_col()
    })
}


# Web application
# ----------------------------------------------------------------------------

# Run the application 
shinyApp(ui = ui, server = server)
